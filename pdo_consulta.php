<?php
try {
  $conn = new PDO('pgsql:dbname=postgres;user=postgres;password=123456789;host=localhost');
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $result = $conn->query('SELECT codigo, nome FROM famosos ');

  if ($result) {
    foreach ($result as $row) {
      print $row['codigo'] . ' - ' . $row['nome'] . "\n";
    }
  }

  $conn = null;
} catch (PDOException $e) {
  print 'Erro -> ' . $e->getMessage();
}
