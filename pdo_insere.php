<?php
try {
  $conn = new PDO('pgsql:dbname=postgres;user=postgres;password=123456789;host=localhost');
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $conn->exec("INSERT INTO famosos (codigo, nome) VALUES (1, 'Érico Veríssimo')");
  $conn->exec("INSERT INTO famosos (codigo, nome) VALUES (2, 'Clarice Lispector')");

  $conn = null;
} catch (PDOException $e) {
  print 'Erro -> ' . $e->getMessage();
}
